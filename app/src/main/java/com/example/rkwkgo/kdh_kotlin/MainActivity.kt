package com.example.rkwkgo.kdh_kotlin

import android.content.Context
import android.graphics.Color
import android.media.AudioManager
import android.media.SoundPool
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import com.google.firebase.iid.FirebaseInstanceId



class MainActivity : AppCompatActivity(){

    var pool: SoundPool? = null
    var resId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_red_card.visibility = View.GONE
        btn_yellow_card.visibility = View.GONE

        pool = SoundPool(1, AudioManager.STREAM_MUSIC, 0)
        resId = pool?.load(this, R.raw.sample, 1)

        var type = intent.getStringExtra("type")
        when(type){
            "Y" -> {
                layout_bg.setBackgroundColor(Color.parseColor("#FFE400"))
                btn_red_card.visibility = View.GONE
                btn_yellow_card.visibility = View.GONE
                resId?.let { pool?.play(it, 1f, 1f, 0, 1, 0.5f) }
            }
            "R" -> {
                layout_bg.setBackgroundColor(Color.parseColor("#FF0000"))
                btn_red_card.visibility = View.GONE
                btn_yellow_card.visibility = View.GONE
                resId?.let { pool?.play(it, 1f, 1f, 0, 1, 2f) }
            }
            else -> {
                layout_bg.setBackgroundColor(Color.parseColor("#ffffff"))
                btn_red_card.visibility = View.VISIBLE
                btn_yellow_card.visibility = View.VISIBLE
            }
        }

    }

    fun onClick(view: View?){
        when(view){
            btn_red_card -> {
                layout_bg.setBackgroundColor(Color.parseColor("#FF0000"))
                btn_red_card.visibility = View.GONE
                btn_yellow_card.visibility = View.GONE
                resId?.let { pool?.play(it, 1f, 1f, 0, 1, 2f) }
            }
            btn_yellow_card -> {
                layout_bg.setBackgroundColor(Color.parseColor("#FFE400"))
                btn_red_card.visibility = View.GONE
                btn_yellow_card.visibility = View.GONE
                resId?.let { pool?.play(it, 1f, 1f, 0, 1, 0.5f) }
            }
            else -> {
                layout_bg.setBackgroundColor(Color.parseColor("#ffffff"))
                btn_red_card.visibility = View.VISIBLE
                btn_yellow_card.visibility = View.VISIBLE
            }
        }
    }

}
