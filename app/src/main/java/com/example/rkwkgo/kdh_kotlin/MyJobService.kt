package com.example.rkwkgo.kdh_kotlin

import android.app.job.JobService
import android.app.job.JobParameters
import android.util.Log


/**
 * Created by rkwkgo on 2017-12-08.
 */
class MyJobService: JobService() {

    private val TAG = "MyJobService"

    override fun onStartJob(jobParameters: JobParameters): Boolean {
        Log.d(TAG, "Performing long running task in scheduled job")
        // TODO(developer): add long running task here.
        return false
    }

    override fun onStopJob(jobParameters: JobParameters): Boolean {
        return false
    }

}